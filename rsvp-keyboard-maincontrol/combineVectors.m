function combinationArray = combineVectors(inputCell)
% combineVectors generates an array with all possible combinations of
% elements of the vectors in the input cell
% Inputs:
%       inputCell               1-D cell with N vectors each one of size
%                               N_i with i=1...N . the vectors are 1 by N_i
%                               vectors (row vectors)
%                   
% Outputs:
%       combinationArray        2-D array of size N_1*...*N_N by N
%                               with the combination of the vector elements
%                               in the input cell
%
% Here is how combinationArray looks 
%
% inputCell{1}(1)   inputCell{2}(1)     ...     inputCell{N}(1) 
% inputCell{1}(2)   inputCell{2}(1)     ...     inputCell{N}(1) 
%   .
%   .
% inputCell{1}(N_1) inputCell{2}(2)     ...     inputCell{N}(1) 
% inputCell{1}(1)   inputCell{2}(2)     ...     inputCell{N}(1)
% inputCell{1}(2)   inputCell{2}(2)     ...     inputCell{N}(1)
%   .
%   .
%   .
% inputCell{1}(N_1) inputCell{2}(N_2)   ...     inputCell{N}(N_N) 
%
% The fastest varying dimension is the first cell vector
% Author: Fernando Quivira
% Date: 9/23/2013
% Version 1.0

% Get dimensions of input vectors
N = cellfun(@numel, inputCell);

% Get total number of permutations
Nt = prod(N);
Nvec = length(N);

% Pre-allocation
combinationArray = zeros(Nt, Nvec);

for idxVector = 1:Nvec
    % This is how body of loop works
    % First vector gets repeated since its the fastest varying one
    % Second vector needs to have each element repeated N_1 times to form a
    % N_1*N_2 vector and then this output will be repeated to fill the
    % combination array
    % So on an so forth
    
    tmp = repmat( inputCell{idxVector} , Nt/prod(N(idxVector:end)), 1);
    tmp = reshape(tmp, [], 1);
    combinationArray(:,idxVector) = repmat(tmp, Nt/prod(N(1:idxVector)), 1);
end

end
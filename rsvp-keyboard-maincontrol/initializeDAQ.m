%% initializeDAQ Script
% initializeDAQ is a script which checks Data Acquisition type and
% accordingly calls appropiate function for data acquisition.

%%
switch RSVPKeyboardParams.DAQType
    case 'gUSBAmp'
        %% Detect connected amplifier(s)
        [success,amplifierStruct]=detectAmps();
        if(success==0),success=0; numberOfChannels =0 ; return,end
        
        %% Calibrate amplifier(s) if daqStruct.calibrationOn is true
        DAQParameters;
        if daqStruct.calibrationOn, calibrateAmps(amplifierStruct); end
        
        %% Set amplifier(s) parameters
        [success,amplifierStruct,fs,numberOfChannels] = ...
            initializeAmps(amplifierStruct);
        if(success==0),return,end
        amplifierStruct.DAQType='gUSBAmp';
    case 'noAmp'
        [success,amplifierStruct,fs,numberOfChannels]=initializeDAQnoAmp;
        amplifierStruct.DAQType='noAmp';
end
function status = exportToExcel(fileName, paramNames, paramCell, dataStruct)
% exportToExcel function exports parameters and data to an Excel
% spreadsheet. The function supports to modes of operation that are
% automatically detected depending on data
% Inputs:
%       fileName                File name of xls file
%       paramNames              1-D cell (N elements) containing the name
%                               of the parameters that were used to generate 
%                               the data. These names will be put on top of
%                               the columns
%       paramCell               1-D cell (N elements) of vectors containing
%                               the parameter values. The size of each
%                               vector can vary depending on mode of
%                               operation
%       dataStruct              Struct with M fields, where each field
%                               contains the data. Each field will be put
%                               in a separate sheet. If the field name is
%                               longer than 31 characters, the last 31
%                               characters will be used for sheet name
%                   
% Outputs:
%       status                  1 if .xls file is generated, 0 otherwise.
%
% 
% Depending on the input, this function follows 2 modes of operation:
%
% Mode 0: the vectors in paramCell are of different sizes
% If so, this function assumes that size of the fields in dataStruct is
% equal to the product of the dimensions of the parameter vectors and that
% the relation occurs in the following way:
%
% paramCell{1}(1) paramCell{2}(1) ... paramCell{N}(1) -> dataStruct.field(1)
% paramCell{1}(2) paramCell{2}(1) ... paramCell{N}(1) -> dataStruct.field(2)
%   .
%   .
%   . 
% paramCell{1}(1) paramCell{2}(2) ... paramCell{N}(1) -> dataStruct.field(N_1+1)
% paramCell{1}(2) paramCell{2}(2) ... paramCell{N}(1) -> dataStruct.field(N_1+2)
%   .
%   .
%   .
% paramCell{1}(N_1) paramCell{2}(N_2) ... paramCell{N}(N_N) -> dataStruct.field(N_1*...*N_N)
%
% In essence, the excel spreadsheet will contain all possible combinations
% of the paramaters with the fastes varying one being the first in the cell
%
% Mode 1:
% There is a one to one correspondence between each parameter value and
% data element. Therefore, all must be of same size
%
% paramCell{1}(i) paramCell{2}(i) ... paramCell{N}(i) -> dataStruct.field(i)
%
% Limitations: function only supports columns from A-Z i.e 25 parameter vectors
%
% Author: Fernando Quivira
% Date: 9/23/2013
% Version 1.0

if nargin < 4
    error('Not enough arguments')
end

if nargin > 4
    error('Too many arguments')
end

% Eliminate warning when writing excel sheets
warning('off','MATLAB:xlswrite:AddSheet');

% Check if all fields in dataStruct are of same size
Ndata = structfun(@numel, dataStruct);
if ~all(Ndata == Ndata(1))
    error('Each field in the dataStruct must have the same numberof elements')
end
Ndata = Ndata(1);

% Compute number of dimension of each parameter vector
Nparam = cellfun(@numel, paramCell);

% Find mode of operation: 
% mode 0 -> number of data poitns equal to product of diemnsions of each
% parameter vector
% mode 1 -> param vectors same size as data
if Ndata == prod(Nparam)
    modeFlag = 0;
elseif all(Nparam == Nparam(1)) && (Ndata == Nparam(1))
    modeFlag = 1;
else
    error('Check that data and parameters are of correct size for mode 0 or mode 1')
end

% Support columns from A->Z
columnLetters = char(65:90);
sheetNames = fieldnames(dataStruct);

% The column of data is at N+1
dataColumn = columnLetters(numel(paramCell) + 1);

% Write depending on mode
switch modeFlag
    case 0
        
        % Create combination of parameter vectors
        paramArray = combineVectors(paramCell);
        
        for idxSheet = 1:numel(sheetNames)
            tmp = dataStruct.(sheetNames{idxSheet});            
            
            % Get sheet name limited to last 31 characters
            sheetName = sheetNames{idxSheet}( max(end-30,1) : end );
            
            % Write parameter names on top of each column
            xlswrite(fileName, paramNames, sheetName, 'A1');
            % Write parameter values right after
            xlswrite(fileName, paramArray, sheetName, 'A2');
            
            % Write field name and data 
            xlswrite(fileName, sheetNames(idxSheet), sheetName, [dataColumn '1']);
            xlswrite(fileName, tmp(:), sheetName, [dataColumn '2']);
        end
        
    case 1
        
        paramArray = reshape(cell2mat(paramCell), Ndata, []);
        
        for idxSheet = 1:numel(sheetNames)
            tmp = dataStruct.(sheetNames{idxSheet});            
            
            % Get sheet name limited to last 31 characters
            sheetName = sheetNames{idxSheet}( max(end-30,1) : end );
            
            % Write parameter names on top of each column
            xlswrite(fileName, paramNames, sheetName, 'A1');
            % Write parameter values right after
            xlswrite(fileName, paramArray, sheetName, 'A2');
            
            % Write field name and data 
            xlswrite(fileName, sheetNames(idxSheet), sheetName, [dataColumn '1']);
            xlswrite(fileName, tmp(:), sheetName, [dataColumn '2']);
        end
        
end

status = exist(fileName, 'file')>0;

end